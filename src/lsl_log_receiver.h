#ifndef LSL_LOG_RECEIVER_H
#define LSL_LOG_RECEIVER_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include "lsl_receiver.h"
#include "lsl_video.h"
#include <QString>

class lsl_log_receiver
{
public:
    lsl_log_receiver();
    bool isNewLogDone();
    void start(std::string device_id, bool CentralCamera);
    void stop();
    std::string& getNewLogString();
    bool isReceiverStarted();
    std::string eye_tracker_id;
    std::string specific_camera_type;

private:
    bool started_receiver;
    bool ena_work;
    bool thr_working;
    bool log_done;
    void log_thread();
    std::string raw_log;
    std::string last_log;
    std::thread* t_log_thread;
};

#endif // LSL_LOG_RECEIVER_H

#include "lsl_log_receiver.h"

lsl_log_receiver::lsl_log_receiver()
{
    started_receiver = false;
    ena_work = true;
    log_done = false;
    thr_working = false;
}

bool lsl_log_receiver::isNewLogDone()
{
    return log_done;
}

void lsl_log_receiver::start(std::string device_id, bool CentralCamera)
{
    ena_work = true;
    log_done = false;
    thr_working = false;
    std::string side_str(".side");
    std::string cent_str(".cent");
    std::string log_name ("eyetracker.log");
    eye_tracker_id = device_id;
    if(CentralCamera)
        specific_camera_type = log_name+cent_str;
    else
        specific_camera_type = log_name+side_str;
    if (!started_receiver)
    {
        t_log_thread = new std::thread(&lsl_log_receiver::log_thread, this);
        started_receiver = true;
    }
}

void lsl_log_receiver::stop()
{
    //mutex
    if (started_receiver)
    {
        ena_work = false;
        t_log_thread->join();
        delete t_log_thread;
        started_receiver = false;
    }
    ena_work = false;
}

std::string& lsl_log_receiver::getNewLogString()
{
    if(log_done)
    {
        if(!thr_working)
            last_log = raw_log;
        log_done = false;
    }
    return last_log;
}

bool lsl_log_receiver::isReceiverStarted()
{
    return started_receiver;
}

void lsl_log_receiver::log_thread()
{
    lsl::variable_video_frame_t log_buffer (1000);
    lsl_receiver<lsl::variable_video_frame_t> log_receiver;
    log_receiver.set_name(specific_camera_type,eye_tracker_id);
    log_receiver.start();

    bool resolved = false;

    while(ena_work)
    {
        resolved = log_receiver.resolve(0.02);
        if(resolved)
        {
            break;
        }
    }

    if(resolved)
    {
        while (ena_work)
        {
            if (log_receiver.receive(log_buffer, 0.0))
            {
                thr_working = true;
                raw_log = *log_buffer.data();
                thr_working = false;
                log_done = true;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
    log_receiver.stop();
}

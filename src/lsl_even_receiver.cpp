#include "lsl_even_receiver.h"

lsl_even_receiver::lsl_even_receiver()
{
    started_receiver = false;
    resolved_signal = false;
    signal_detected = false;
    freq = 0.0;
    ena_work = true;
    prev_ts = 0.0;
}

void lsl_even_receiver::start(std::string device_id)
{
    tm1.stop(); tm1.reset();
    signal_detected = false;
    ena_work = true;
    resolved_signal = false;
    eye_tracker_id = device_id;
    //mutex
    if (!started_receiver)
    {
        thread_p = new std::thread(&lsl_even_receiver::main_thread, this);
        started_receiver = true;
    }
}

void lsl_even_receiver::stop()
{
    tm1.stop();
    freq = 0.0;
    resolved_signal = false;
    signal_detected = false;
    ena_work = false;
    if (started_receiver)
    {
        thread_p->join();
        delete thread_p;
        started_receiver = false;
    }
    ena_work = false;
}

void lsl_even_receiver::main_thread()
{
    lsl::events_t event_obj;
    lsl_receiver<lsl::events_t> ev_receiver;
    ev_receiver.set_name("eyetracker.events",eye_tracker_id);
    ev_receiver.start();

    bool resolved = false;

    while(ena_work)
    {
        resolved = ev_receiver.resolve(0.2);
        if(resolved)
        {
            break;
        }
    }

    if(resolved)
    {
        resolved_signal = true;
        while (ena_work)
        {
            tm1.stop();
            if(tm1.getTimeMilli()>5200) signal_detected = false;
            tm1.start();
            if (ev_receiver.receive(event_obj, 0.0))
            {
                resolved_string = std::string(*event_obj.data());
                double delta = event_obj.timestamp - prev_ts;
                prev_ts = event_obj.timestamp;
                freq = 1.0/delta;
                signal_detected = true;
                tm1.reset();
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
        }
    }
    ev_receiver.stop();
}


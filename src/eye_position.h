#ifndef EYE_POSITION_H
#define EYE_POSITION_H

#include <string>
#include "lsl_cpp_wrapper.h"


//position_params_t
//eye_position_t

namespace lsl {


// et_main -> central_cam

class position_params_t
{
public:
    static const int channels = 14;

    union {
        struct {
            float eye_left_x;               // координаты левого глаза
            float eye_left_y;

            float eye_right_x;              // координаты правого глаза
            float eye_right_y;

            float center_point_x;           // координаты точки между глаз
            float center_point_y;

            float eye_left_x_reflex;        // координаты усреднённого блика левого глаза
            float eye_left_y_reflex;

            float eye_right_x_reflex;       // координаты усреднённого блика правого глаза
            float eye_right_y_reflex;

            float center_point_x_reflex;    // усреднённое значение бликов левого и правого глаза
            float center_point_y_reflex;

            float eye_left_distance;        // оценка дистанции, мм
            float eye_right_distance;       // оценка дистанции, мм
        };
        float buf[channels];
    };

    double timestamp = 0.0;


public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate)
    {
        lsl::stream_info info(name, "undefined", channels, sample_rate, lsl::cf_float32, src_id);
        info.desc().append_child_value("version", "2.0");
        return info;
    }
    float* data() { return buf; }
    const float* cdata() const { return buf; }


    void invalidate()
    {
        for (int i=0; i<channels; ++i)
            buf[i] = -1.0f;
    }

};




//-----------------------------------------------------------

typedef struct {
    float flREFLEXx_1;  // пиксельные координаты блика 1
    float flREFLEXy_1;

    float flREFLEXx_2;  // пиксельные координаты блика 2
    float flREFLEXy_2;

    float flPUPILx;     // пиксельные координаты зрачка
    float flPUPILy;

    float flPUPILr;     // радиус зрачка

    void invalidate()
    {
        flREFLEXx_1 = -1.0f;
        flREFLEXy_1 = -1.0f;
        flREFLEXx_2 = -1.0f;
        flREFLEXy_2 = -1.0f;
        flPUPILx = -1.0f;
        flPUPILy = -1.0f;
        flPUPILr = -1.0f;
    }

} eye_params_t;



//class pupil_reflex_t
class all_eyes_params_t
{
public:
    static const int channels = 7+7 +1;

    union {
        struct {
            eye_params_t left;
            eye_params_t right;
            uint32_t frame_number;
        };
        float buf[channels];
    };

    double timestamp = 0.0;

public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate)
    {
        lsl::stream_info info(name, "undefined", channels, sample_rate, lsl::cf_float32, src_id);
        info.desc().append_child_value("version", "1.0");
        return info;
    }
    float* data() { return buf; }
    const float* cdata() const { return buf; }

    //frame_number and timestamp are intentionally not changed
    void invalidate()
    {
        left.invalidate();
        right.invalidate();
    }

    //void clear();
    bool is_right_blink_detected(int eye) const    // left: eye=0
    {
        if (eye == 0)
            return (left.flREFLEXx_1 != -1 && left.flREFLEXy_1 != -1);
        else
            return (right.flREFLEXx_1 != -1 && right.flREFLEXy_1 != -1);

    }
    bool is_left_blink_detected(int eye) const     // left: eye=0
    {
        if (eye == 0)
            return (left.flREFLEXx_2 != -1 && left.flREFLEXy_2 != -1);
        else
            return (right.flREFLEXx_2 != -1 && right.flREFLEXy_2 != -1);
    }

    bool is_pupil_detected(int eye) const          // left: eye=0
    {
        if (eye == 0)
            return (left.flPUPILx != -1 && left.flPUPILy != -1);
        else
            return (right.flPUPILx != -1 && right.flPUPILy != -1);
    }

};







// ----

class roi_position_t
{
public:
    static const int channels = 10;

    union {
        struct {
            float rect_x;                   // pixels, ROI rectangle
            float rect_y;
            float rect_width;
            float rect_height;

            float eye_right_x;              // pixels
            float eye_right_y;
            float eye_left_x;
            float eye_left_y;

            float eye_right_distance;       // mm
            float eye_left_distance;        // mm
        };
        float buf[channels];
    };

    double timestamp = 0.0;

public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate)
    {
        lsl::stream_info info(name, "roi_position", channels, sample_rate, lsl::cf_float32, src_id);
        info.desc().append_child_value("version", "1.0");
        return info;
    }
    float* data() { return buf; }
    const float* cdata() const { return buf; }

    //void clear();
};



} // namespace lsl



#endif // EYE_POSITION_H

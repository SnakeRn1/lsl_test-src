#ifndef LSL_EVEN_RECEIVER_H
#define LSL_EVEN_RECEIVER_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include "lsl_receiver.h"
#include "events.h"

class lsl_even_receiver
{

public:
    lsl_even_receiver();

    void start(std::string device_id);
    void stop();
    bool isResolved() {return resolved_signal;}
    bool isDetected() {return signal_detected;}
    double get_freq() {return freq;}

private:
    double freq;
    double prev_ts;
    cv::TickMeter tm1;

    bool ena_work;
    bool started_receiver;
    bool resolved_signal;
    bool signal_detected;

    std::string resolved_string;
    std::string eye_tracker_id;
    std::thread* thread_p;

    void main_thread();
};

#endif // LSL_EVEN_RECEIVER_H

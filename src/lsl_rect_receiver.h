#ifndef LSL_RECT_RECEIVER_H
#define LSL_RECT_RECEIVER_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include "lsl_receiver.h"
#include "eye_position.h"
#include <QImage>
class lsl_rect_receiver
{
public:
    lsl_rect_receiver();
    bool isNewRectDone();
    void start(std::string device_id);
    void stop();
    bool isReceiverStarted();
    cv::Mat& getNewRawImg();
    cv::Mat& getNewResizedImg(int wi,int he);
    int width;
    int height;
    float decimator;
    std::string eye_tracker_id;
    static void et_print(cv::Mat &img, int lineOffsY, const std::string &ss);
    cv::Mat frame_rect_raw;
    QImage m_qt_img_rect;

private:
    cv::Size raw_frame_size;
    int video_cnt;
    int video_codec;
    cv::VideoWriter writer_rect;
    bool started_receiver;
    bool ena_work;
    bool thr_working;
    cv::Mat frame_rect_maxi;
    bool rect_done;
    void rect_thread();
    std::thread* t_rect_thread;
    cv::Mat last_frame_rect;
    void matPrint(cv::Mat &img, int lineOffsY, const std::string &ss);
};

#endif // LSL_RECT_RECEIVER_H

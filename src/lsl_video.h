#ifndef LSL_VIDEO_H
#define LSL_VIDEO_H

#include <string>
#include "lsl_cpp_wrapper.h"

namespace lsl {


typedef struct variable_video_frame_descr {
    // "encoding"
    int width;
    int height;
    int color_channels;
    std::string codec;
    int compression_quality;
    int decimation_hor;
    int decimation_ver;
} variable_video_frame_descr_t;




class variable_video_frame_t
{
public:
    variable_video_frame_t(size_t max_size);


    // поместить данные: безопасно, но с копированием
    void push(const char* p, size_t n);

    // поместить данные: опасно, но эффективно, без копирования
    char* get_ptr() const { return const_cast<char*>(s_.data()); }
    void  set_size(size_t n) { s_.resize(n); }


    // можно попросить сделать описатель каналов из предоставленной информации.
    // этот описатель можно потом отдать sender
    static lsl::xml_element make_description(const variable_video_frame_descr_t & params);


public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate);
    std::string* data() { return &s_; }
    const std::string* cdata() const { return &s_; }

public:
    static const int channels = 1;
    double timestamp = 0.0;

private:
    std::string s_;
    size_t max_size_;
};




} //namespace lsl




#endif // LSL_VIDEO_H

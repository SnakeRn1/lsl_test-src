#include "lsl_eyes_receiver.h"

lsl_eyes_receiver::lsl_eyes_receiver()
{
    started_receiver = false;
    started_video_saving = false;
    ena_work = true;
    done = false;
    thr_working = false;
    video_working = false;
    raw_frame_size = cv::Size(200,100);
    frame_raw = cv::Mat(raw_frame_size,CV_8UC1,cv::Scalar(0));
    raw_view_size = cv::Size(400,200);
    qt_img = QImage(QSize(raw_view_size.width,raw_view_size.height),QImage::Format_Grayscale8);
    frame_view = cv::Mat(raw_view_size,CV_8UC1,qt_img.bits());
    frame_view.setTo(0);
    video_codec = cv::VideoWriter::fourcc('X','V','I','D');
    video_cnt = 0;
}

bool lsl_eyes_receiver::isNewFrameDone()
{
    return done;
}

void lsl_eyes_receiver::start(std::string device_id)
{
    ena_work = true;
    done = false;
    thr_working = false;
    video_working = false;
    resolved_signal = false;
    eye_tracker_id = device_id;
    //mutex
    if (!started_receiver)
    {
        thread_p = new std::thread(&lsl_eyes_receiver::main_thread, this);
        started_receiver = true;
    }
}

bool lsl_eyes_receiver::startVideoSaving ()
{
    if(!started_video_saving&&!writer.isOpened())
    {
        std::string filename = eye_tracker_id+"number"+std::to_string(video_cnt)+"_last_saved_video_eyes.mkv";
        if(writer.open(filename,video_codec,10,raw_view_size,false))
        {
            started_video_saving = true;
            return true;
        }
        else
            return false;
    }
    else return false;
}

void lsl_eyes_receiver::stopVideoSaving ()
{
    started_video_saving = false;
    while(video_working)
    {
        std::cout<<"now_waiting_video_saving"<<std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    if(writer.isOpened())
        writer.release();
    video_cnt ++;
}

void lsl_eyes_receiver::stop()
{
    //mutex
    resolved_signal = false;
    done = false;
    ena_work = false;
    if (started_receiver)
    {
        thread_p->join();
        delete thread_p;
        started_receiver = false;
    }
    if(started_video_saving)
    {
        if(writer.isOpened())
            writer.release();
        started_video_saving = false;
        video_cnt ++;
    }
    ena_work = false;
}

bool lsl_eyes_receiver::isVideoStarted()
{
    return started_video_saving;
}

bool lsl_eyes_receiver::isReceiverStarted()
{
    return started_receiver;
}

bool lsl_eyes_receiver::isResolved()
{
    return resolved_signal;
}

void lsl_eyes_receiver::main_thread()
{
    lsl::variable_video_frame_t frame(388800);

    lsl_receiver<lsl::variable_video_frame_t> cam_video;
    cam_video.set_name("eyetracker.eyes.video",eye_tracker_id);
    cam_video.start();

    bool resolved = false;

    while(ena_work)
    {
        resolved = cam_video.resolve(0.2);
        if(resolved)
        {
            break;
        }
    }

    if(resolved)
    {
        resolved_signal = true;
        while (ena_work)
        {
            if (cam_video.receive(frame, 0.0))
            {
                thr_working = true;
                std::string* s = frame.data();
                char* data = const_cast<char*>(s->data());




                frame_raw.data = reinterpret_cast<uchar*>(data);
                resize(frame_raw,frame_view,raw_view_size);
                thr_working = false;
                if(started_video_saving)
                {
                    video_working = true;
                    writer<<frame_view;
                    video_working = false;
                }
                if(ena_work) done = true;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
        }
    }
    cam_video.stop();
}

QImage& lsl_eyes_receiver::getLastImage()
{
    if(!thr_working)
    {
        return qt_img;
    }
    else
    {
        while(thr_working) std::this_thread::sleep_for(std::chrono::milliseconds(1));
        return qt_img;
    }
}

#include "choose.h"
#include "ui_choose.h"
#include "QMessageBox"

using namespace std;
using namespace cv;

Choose::Choose(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Choose)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    ui->alfhabetCombo->installEventFilter(this);
    ui->group_spin->installEventFilter(this);
    ui->user_spin->installEventFilter(this);
    cursorRed = QCursor(QPixmap(":res/red"),0,0);
    cursorGreen = QCursor(QPixmap(":res/grn"),0,0);
    cursorTarget = QCursor(QPixmap(":res/tar"));
    this->setCursor(cursorRed);

    ui->alfhabetCombo->setCursor(cursorGreen);
    ui->rec_button->setCursor(cursorGreen);
    ui->choose_button->setCursor(cursorGreen);
    ui->next_button->setCursor(cursorGreen);
    ui->prev_button->setCursor(cursorGreen);
    ui->back_cam_power->setCursor(cursorGreen);
    face = true;
    eyes = true;
    back = true;
    even = true;
    back_on = true;
    fullscreen = false;
    value_now = "0";
    on_choose_button_clicked();
    m_timer = new QTimer(this);
    connect(m_timer,SIGNAL(timeout()),this,SLOT(tim_func()));
    m_timer->start(static_cast<int>(10));
}

Choose::~Choose()
{
    delete ui;
}

bool Choose::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == ui->alfhabetCombo)
    {
        if(event->type() == QEvent::MouseMove)
        {
            QMouseEvent* e = static_cast<QMouseEvent*>(event);
            if(e->buttons() == Qt::LeftButton)
            {
                return true;
            }
        }
    }
    if(watched == ui->group_spin)
    {
        if(event->type() == QEvent::KeyPress)
        {
            QKeyEvent* e = static_cast<QKeyEvent*>(event);
            if(e->key() == Qt::Key_Return)
            {
                QString group_temp;
                if(ui->g_selector_box->isChecked()) group_temp = "Group";
                else group_temp = "group";
                device = group_temp+ui->group_spin->text()+".user"+ui->user_spin->text();
                if(ui->choose_button->isEnabled()) reselect_et();
            }
        }
    }
    if(watched == ui->alfhabetCombo)
    {
        if(event->type() == QEvent::KeyPress)
        {
            QKeyEvent* e = static_cast<QKeyEvent*>(event);
            if(e->key() == Qt::Key_Return)
            {
                QString group_temp;
                if(ui->g_selector_box->isChecked()) group_temp = "Group";
                else group_temp = "group";
                device = "group"+ui->alfhabetCombo->currentText()+".user"+ui->user_spin->text();
                if(ui->choose_button->isEnabled()) reselect_et();
            }
        }
    }
    if(watched == ui->user_spin)
    {
        if(event->type() == QEvent::KeyPress)
        {
            QKeyEvent* e = static_cast<QKeyEvent*>(event);
            if(e->key() == Qt::Key_Return)
            {
                QString group_temp;
                if(ui->g_selector_box->isChecked()) group_temp = "Group";
                else group_temp = "group";
                device = "group"+value_now+".user"+ui->user_spin->text();
                if(ui->choose_button->isEnabled()) reselect_et();
            }
        }
    }
    return false;
}

void Choose::mouseMoveEvent( QMouseEvent* e )
{
    if((e->buttons() == Qt::LeftButton)&&(!fullscreen))
    {
        setCursor(cursorTarget);
        move(e->globalPos()-d);
    }
}

void Choose::mousePressEvent( QMouseEvent* e )
{
    if((e->buttons() == Qt::LeftButton)&&(!fullscreen)) d = e->pos();
}

void Choose::mouseReleaseEvent( QMouseEvent* e )
{
    e->accept();
    setCursor(cursorRed);
}

void Choose::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr(""), tr("Закрыть все приёмники и выйти?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        event->accept();
        if (ret == QMessageBox::Yes)
        {
            this->close();
        }
    }
}

void Choose::closeEvent(QCloseEvent *event)
{
    release_all_receivers();
    event->accept();
}

void Choose::mouseDoubleClickEvent(QMouseEvent *event)
{
    event->accept();
    fullscreen = !fullscreen;
    if(fullscreen) this->showFullScreen();
    else this->showNormal();
}

void Choose::release_all_receivers()
{
    receiver_even.stop();
    receiver_face.stop();
    receiver_eyes.stop();
    receiver_back.stop();
}

void Choose::on_alfhabetCombo_currentTextChanged(const QString &arg1)
{
    value_now = arg1;
}

void Choose::on_group_spin_valueChanged(const QString &arg1)
{
    value_now = arg1;
}

void Choose::on_choose_button_clicked()
{
    QString group_temp;
    if(ui->g_selector_box->isChecked()) group_temp = "Group";
    else group_temp = "group";
    device = group_temp+value_now+".user"+QString::number(ui->user_spin->value());
    reselect_et();
}

void Choose::on_want_window_returnPressed()
{
    QString want_request = ui->want_window->text();
    if(want_request.contains("roup")&&want_request.contains("ser"))
    {
        ui->want_window->clear();
        ui->special_et->setText("Успешно");
        device = want_request;
        reselect_et();
    }
    else
    {
        ui->special_et->setText("Неверно");
    }
}

void Choose::new_eyes_thread()
{
    eyes = false;
    string device2(device.toStdString());
    eyes_str = "Обновление";
    receiver_eyes.stop();
    receiver_eyes.start(device2);
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));
    if(!receiver_eyes.isResolved()) eyes_str = "Нет сигнала видео глаз";
}

void Choose::new_even_thread()
{
    even = false;
    string device2(device.toStdString());
    even_str = "Обновление";
    receiver_even.stop();
    receiver_even.start(device2);
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));
    if(!receiver_even.isResolved()) even_str = "Нет сигнала потока событий";
    else even_str = "Поток событий в наличии";
}

void Choose::new_face_thread()
{
    face = false;
    string device2(device.toStdString());
    face_str = "Обновление";
    receiver_face.stop();
    receiver_face.start(device2);
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));
    if(!receiver_face.isResolved()) face_str = "Нет сигнала видео лица";
}

void Choose::new_back_thread()
{
    back = false;
    if(back_on)
    {
        string device2(device.toStdString());
        back_str = "Обновление";
        receiver_back.stop();
        receiver_back.start(device2);
        std::this_thread::sleep_for(std::chrono::milliseconds(1500));
        if(!receiver_back.isResolved()) back_str = "Нет сигнала видео тыльной камеры";
    }
    else
    {
        ui->back->setText("Отключено");
    }
}

void Choose::new_all_thread()
{
    QString s1 = "Поиск " + device + "...";
    QString s2 = "Запись видео недоступна";
    ui->stat_et->setText(s1);
    ui->stat_video->setText(s2);

    reselect1 = new std::thread(&Choose::new_face_thread,this);
    reselect2 = new std::thread(&Choose::new_eyes_thread,this);
    reselect3 = new std::thread(&Choose::new_back_thread,this);
    reselect4 = new std::thread(&Choose::new_even_thread,this);

    reselect1->join();
    reselect2->join();
    reselect3->join();
    reselect4->join();

    delete reselect1;
    delete reselect2;
    delete reselect3;
    delete reselect4;

    if (!receiver_face.isResolved()&&!receiver_eyes.isResolved())
    {
        QString s1 = device + " недоступен";
        QString s2 = "Запись видео недоступна";
        ui->stat_et->setText(s1);
        ui->stat_video->setText(s2);
    }
    else
    {
        QString s1 = "В работе " + device;
        QString s2 = "Возможна запись видео";
        ui->stat_et->setText(s1);
        ui->stat_video->setText(s2);
        ui->rec_button->setEnabled(true);
    }

    ui->next_button->setEnabled(true);
    ui->prev_button->setEnabled(true);
    ui->choose_button->setEnabled(true);
    ui->back_cam_power->setEnabled(true);
}

void Choose::reselect_et()
{
    ui->rec_button->setEnabled(false);
    ui->choose_button->setEnabled(false);
    ui->next_button->setEnabled(false);
    ui->prev_button->setEnabled(false);
    ui->back_cam_power->setEnabled(false);

    reselect_all = new std::thread(&Choose::new_all_thread,this);
}

void Choose::on_rec_button_clicked()
{
    if(receiver_face.isVideoStarted()) receiver_face.stopVideoSaving();
    else receiver_face.startVideoSaving();

    if(receiver_eyes.isVideoStarted()) receiver_eyes.stopVideoSaving();
    else receiver_eyes.startVideoSaving();

    if(back_on)
    {
        if(receiver_back.isVideoStarted()) receiver_back.stopVideoSaving();
        else receiver_back.startVideoSaving();
    }

    if(receiver_face.isVideoStarted())
    {
        QString i("Запись видео");
        ui->stat_video->setText(i);
        ui->choose_button->setEnabled(false);
        ui->rec_button->setText("Окончание записи");
    }
    else
    {
        QString i("Возможна запись видео");
        ui->stat_video->setText(i);
        ui->choose_button->setEnabled(true);
        ui->rec_button->setText("Запись видео");
    }
}

void Choose::on_back_cam_power_clicked()
{
    ui->back_cam_power->setEnabled(false);
    back_on = !back_on;
    if(back_on)
    {
        reselect3 = new std::thread(&Choose::new_back_thread,this);
        ui->back_cam_power->setText("Выкл Тыл");
    }
    else
    {
        receiver_back.stop();
        ui->back_cam_power->setText("Вкл Тыл");
        ui->back->setText("Отключено");
    }
    ui->back_cam_power->setEnabled(true);
}

void Choose::tim_func ()
{
    back = receiver_back.isResolved();
    even = receiver_even.isResolved();
    eyes = receiver_eyes.isResolved();
    face = receiver_face.isResolved();

    ui->enabled_back->setEnabled(back);
    ui->enabled_even->setEnabled(even);
    ui->enabled_eyes->setEnabled(eyes);
    ui->enabled_face->setEnabled(face);
    ui->enabled_laux->setEnabled(receiver_face.isResolvedAux());

    if(!back_str.isEmpty()) {ui->back->setText(back_str); back_str.clear();}
    if(!eyes_str.isEmpty()) {ui->eyes->setText(eyes_str); eyes_str.clear();}
    if(!face_str.isEmpty()) {ui->face->setText(face_str); face_str.clear();}
    if(!even_str.isEmpty()) {ui->stat_events->setText(even_str); even_str.clear();}

    if(receiver_eyes.isNewFrameDone()&&eyes)
    {
        m_qt_pm_eyes = QPixmap::fromImage(receiver_eyes.getLastImage());
        ui->eyes->setPixmap(m_qt_pm_eyes);
    }

    if(receiver_face.isNewFrameDone()&&face)
    {
        m_qt_pm_face = QPixmap::fromImage(receiver_face.getLastImage());
        ui->face->setPixmap(m_qt_pm_face);
    }

    if(receiver_back.isNewFrameDone()&&back&&back_on)
    {
        m_qt_pm_back = QPixmap::fromImage(receiver_back.getLastImage().rgbSwapped());
        ui->back->setPixmap(m_qt_pm_back);
    }

    if(even&&receiver_even.isResolved())
    {
        double last_freq = receiver_even.get_freq();
        if(!receiver_even.isDetected())
        {
            ui->stat_events->setText("Не зафиксированы");
            ui->even_num->display(0);
        }
        else if ((last_freq>0.1)&&(last_freq<9.8))
        {
            ui->stat_events->setText("Зафиксированы");
            ui->even_num->display(last_freq);
        }
        else if ((last_freq>=9.8)&&(last_freq<=200.0))
        {
            ui->stat_events->setText("Зафиксированы");
            ui->even_num->display(cvRound(last_freq));
        }
    }
}

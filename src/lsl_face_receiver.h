#ifndef LSL_FACE_RECEIVER_H
#define LSL_FACE_RECEIVER_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include "lsl_receiver.h"
#include "lsl_video.h"
#include <QImage>
#include <landmarks.h>

class lsl_face_receiver
{
public:
    lsl_face_receiver();

    void start(std::string device_id);
    void stop();
    bool startVideoSaving();
    void stopVideoSaving();

    bool isNewFrameDone() {return done;}
    bool isVideoStarted() {return started_video_saving;}
    bool isReceiverStarted() {return started_receiver;}
    bool isResolved() {return resolved_signal;}
    bool isResolvedAux() {return resolved_aux;}

    QImage& getLastImage();

private:
    QImage qt_img;
    std::string eye_tracker_id;
    cv::Size raw_frame_size;
    cv::Size raw_view_size;
    cv::Mat frame_raw;
    cv::Mat frame_view;
    int video_cnt;
    int video_codec;
    cv::VideoWriter writer;
    bool resolved_signal;
    bool started_receiver;
    bool started_video_saving;
    bool ena_work;
    bool thr_working;
    bool video_working;
    bool done;
    void main_thread();
    std::thread* thread_p;

    bool resolved_aux;
};

#endif

#include "lsl_video.h"
#include <iostream>



namespace lsl {

using namespace std;


variable_video_frame_t::variable_video_frame_t(size_t max_size)
    : max_size_(max_size)
{
    s_.reserve(max_size);
}


void variable_video_frame_t::push(const char *p, size_t n)
{
    if (n > max_size_) {
        cerr << "!!! ERROR" << endl;
        return;
    }
    s_.assign(p, n);
}


//static
lsl::stream_info variable_video_frame_t::make_info(const std::string & name, const std::string & src_id, int sample_rate)
{
    lsl::stream_info info(name, "video", 1, sample_rate, lsl::cf_string, src_id);
    info.desc().append_child_value("version", "1.0");
    return info;
}

//static
lsl::xml_element variable_video_frame_t::make_description(const variable_video_frame_descr_t & params)
{
    //params
    lsl::xml_element desc;
    desc.append_child("encoding")
            .append_child_value("width", std::to_string(params.width))
            .append_child_value("height", std::to_string(params.height))
            .append_child_value("color_channels", std::to_string(params.color_channels))
            .append_child_value("codec", params.codec)
            .append_child_value("compression_quality", std::to_string(params.compression_quality))
            .append_child_value("decimation_hor", std::to_string(params.decimation_hor))
            .append_child_value("decimation_ver", std::to_string(params.decimation_ver));
    return desc;
}




} // namespace lsl


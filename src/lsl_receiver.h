#ifndef LSL_RECEIVER_H
#define LSL_RECEIVER_H

#include <iostream>
#include <mutex>
#include <iostream>
#include <thread>
#include "lsl_cpp_wrapper.h"

template <class T>
class lsl_receiver
{
public:
    lsl_receiver() {}
    lsl_receiver(const std::string & name, const std::string & source_id);
    ~lsl_receiver() { stop(); }

    // config
    void set_name(const std::string & name, const std::string & source_id);

    bool resolve(double timeout =lsl::FOREVER);
    //void cancel_resolve() {resolve_canceled_ = true;}

    bool start();       // enable receiving data
    void stop();        // abort receive() and resolve fuctions, and disable next receiving data

    bool receive(T& sample, double timeout =lsl::FOREVER);

private:
    lsl::stream_inlet * inlet_ = nullptr;
    std::string full_name_;
    std::string source_id_;

    bool resolved_ = false;
    bool started_  = false;

    lsl::stream_info info_;
    std::mutex mtx_;
};



template<class T>
lsl_receiver<T>::lsl_receiver(const std::string & name, const std::string & source_id)
    : full_name_(source_id + "-" + name)
    , source_id_(source_id)
{
}


template<class T>
void lsl_receiver<T>::set_name(const std::string & name, const std::string & source_id)
{
    source_id_ = source_id;
    full_name_ = source_id + "-" + name;
}


template<class T>
bool lsl_receiver<T>::resolve(double timeout /*=lsl::FOREVER*/)
{
    if (resolved_)
        return true;

    if (full_name_.empty() || source_id_.empty())
        return false;

    std::vector<lsl::stream_info> info;

    while (started_) {

        //std::cout << "### waiting lsl receiver: "<<full_name_ << " ###" << std::endl;

        double timeout_portion = (timeout > 5.0) ? 5.0 : timeout;
        info = lsl::resolve_stream("name", full_name_, 1, timeout_portion);
        if (!info.empty())
            break;
        timeout -= timeout_portion;
        if (timeout <= 0)
            break;
    }

    if (info.empty() || !started_)
        return false;


    std::lock_guard<std::mutex> lock(mtx_);
    if (!started_)
        return false;

    //std::cout<<"### lsl receiver: "<<full_name_<<" is opened!!! ###"<<std::endl;
    //std::cout << info[0].as_xml() << std::endl;
    delete inlet_;
    inlet_ = nullptr;
    inlet_ = new lsl::stream_inlet(info[0]);
    info_ = inlet_->info();
    //std::cout << info_.as_xml() << std::endl;
    return resolved_ = true;
}



template<class T>
bool lsl_receiver<T>::start()
{
    std::lock_guard<std::mutex> lock(mtx_);

    started_ = true;
    return started_;
}


template <class T>
void lsl_receiver<T>::stop()
{
    std::lock_guard<std::mutex> lock(mtx_);

    started_ = false;
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    if (resolved_) {
        inlet_->close_stream();
        delete inlet_;
        inlet_ = nullptr;
        resolved_ = false;
    }
}


// timeout The timeout for this operation, if any.  Use 0.0 to make the function non-blocking.
// may be canceled by stop()
template <class T>
bool lsl_receiver<T>::receive(T& sample, double timeout /*=lsl::FOREVER*/)
{
    if (!started_ || !resolved_)
        return false;

    try {

        double ts = 0;
        while (started_) {
            double timeout_portion = (timeout > 0.1) ? 0.1 : timeout;
            ts = inlet_->pull_sample(sample.data(), sample.channels, timeout_portion);
            if (ts != 0)
                break;
            timeout -= timeout_portion;
            if (timeout <= 0)
                break;
        }
        if (!started_ || ts == 0)
            return false;

        sample.timestamp = ts;
        return true;
    }

    catch (std::exception &e) {
        // lost_error
        std::cerr << "lsl_receiver::receive() got an exception: " << e.what() << std::endl;
        return false;
    }

}




#endif // LSL_RECEIVER_H

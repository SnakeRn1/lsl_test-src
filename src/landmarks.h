#ifndef LANDMARKS_H
#define LANDMARKS_H

#include <string>
#include "lsl_cpp_wrapper.h"



namespace lsl {


class landmarks_t
{
public:
    static const int LANDMARKS_CHANNELS = 68*2;
    static const int channels = LANDMARKS_CHANNELS+1;

    union {
        struct {
            float landmarks_buf[LANDMARKS_CHANNELS];
            uint32_t frame;     // номер кадра
        };
        float buf[channels];
    };

    double timestamp = 0.0;


public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate);
    float* data() { return buf; }
    const float* cdata() const { return buf; }

    uint32_t frame_number() const { return frame; }
    void clear();
};




class landmarks_aux_t
{
public:
    static const int channels = 22;

    union {
        struct {
            float right_eye_center_x;       // центр правого глаза
            float right_eye_center_y;
            float between_eyes_center_x;    // середина между глаз (применяется для оценки ROI)
            float between_eyes_center_y;
            float left_eye_center_x;        // центр левого глаза
            float left_eye_center_y;

            float right_eye_reflex1_x;      // блик1 правого глаза
            float right_eye_reflex1_y;
            float left_eye_reflex1_x;       // блик1 левого глаза
            float left_eye_reflex1_y;

            float right_eye_reflex2_x;      // блик2 правого глаза
            float right_eye_reflex2_y;
            float left_eye_reflex2_x;       // блик2 левого глаза
            float left_eye_reflex2_y;

            float distance;                 // оценка дистанции (также называем Dгрубое)
            uint32_t frame;                 // номер кадра

            float head_pose_t_x;            // координаты головы, относительно оптического центра боковой камеры
            float head_pose_t_y;            // return the current estimate of the head pose in world coordinates with camera at origin (0,0,0)
            float head_pose_t_z;            // позиция головы в углах эйлера, относительно оптической оси боковой камеры

            float head_pose_eul_x;          // позиция головы в углах эйлера, относительно оптической оси боковой камеры
            float head_pose_eul_y;
            float head_pose_eul_z;
        };
        float buf[channels];
    };

    double timestamp = 0.0;


public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate);
    float* data() { return buf; }
    const float* cdata() const { return buf; }


    uint32_t frame_number() const { return frame; }
    void clear();

    void invalidate();
    bool is_eye_detected(int eye) const;          // left: eye=0
    bool is_right_blink_detected(int eye) const;  // left: eye=0
    bool is_left_blink_detected(int eye) const;   // left: eye=0
};



// bv, indexes are DEPRECATED, go to use structure fields

enum {
    /* ONLY */   /*TRUE*/    /*ENUM*/
    /* FOR  */   /*INDEX*/   /*NAME*/
    /* SIZE */

    /*1*/   /*0*/   RIGHT_EYE_CENTER_X, // центр правого глаза
    /*2*/   /*1*/   RIGHT_EYE_CENTER_Y,

    /*3*/   /*2*/   BETWEEN_EYES_CENTER_X, // середина между глаз (применяется для оценки ROI)
    /*4*/   /*3*/   BETWEEN_EYES_CENTER_Y,

    /*5*/   /*4*/   LEFT_EYE_CENTER_X, // центр левого глаза
    /*6*/   /*5*/   LEFT_EYE_CENTER_Y,

    /*7*/   /*6*/   RIGHT_EYE_REFLEX1_X, // блик1 правого глаза
    /*8*/   /*7*/   RIGHT_EYE_REFLEX1_Y,
    /*9*/   /*8*/   LEFT_EYE_REFLEX1_X,  // блик1 левого глаза
    /*10*/  /*9*/   LEFT_EYE_REFLEX1_Y,

    /*11*/  /*10*/  RIGHT_EYE_REFLEX2_X, // блик2 правого глаза
    /*12*/  /*11*/  RIGHT_EYE_REFLEX2_Y,
    /*13*/  /*12*/  LEFT_EYE_REFLEX2_X,  // блик2 левого глаза
    /*14*/  /*13*/  LEFT_EYE_REFLEX2_Y,

    /*15*/  /*14*/  DISTANCE,     // оценка дистанции (также называем Dгрубое)
    /*16*/  /*15*/  FRAME_NUMBER, // номер кадра

    /*17*/  /*16*/  HEAD_POSE_T_X, // Координаты головы, относительно оптического центра боковой камеры
    /*18*/  /*17*/  HEAD_POSE_T_Y, // Return the current estimate of the head pose in world coordinates with camera at origin (0,0,0)
    /*19*/  /*18*/  HEAD_POSE_T_Z,// The format returned is [Tx, Ty, Tz, Eul_x, Eul_y, Eul_z]

    /*20*/  /*19*/  HEAD_POSE_EUL_X, // Позиция головы в углах эйлера, относительно оптической оси боковой камеры
    /*21*/  /*20*/  HEAD_POSE_EUL_Y,
    /*22*/  /*21*/  HEAD_POSE_EUL_Z
};








} // namespace lsl


#endif // LANDMARKS_H

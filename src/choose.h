#ifndef CHOOSE_H
#define CHOOSE_H

#include <QWidget>
#include <QString>
#include <QKeyEvent>
#include <QTimer>
#include <QPixmap>
#include <string>
#include <iostream>
#include "lsl_face_receiver.h"
#include "lsl_back_receiver.h"
#include "lsl_eyes_receiver.h"
#include "lsl_even_receiver.h"

namespace Ui {
class Choose;
}

class Choose : public QWidget
{
    Q_OBJECT

public:
    explicit Choose(QWidget *parent = nullptr);
    ~Choose();

private slots:
    void on_choose_button_clicked();
    void on_rec_button_clicked();
    void on_want_window_returnPressed();
    void on_back_cam_power_clicked();
    void tim_func ();
    void on_alfhabetCombo_currentTextChanged(const QString &arg1);
    void on_group_spin_valueChanged(const QString &arg1);
    void mouseMoveEvent( QMouseEvent* e );
    void mousePressEvent( QMouseEvent* e );
    void mouseReleaseEvent( QMouseEvent* e );

private:
    Ui::Choose *ui;

    QCursor cursorRed;
    QCursor cursorGreen;
    QCursor cursorTarget;

    QString value_now;

    void new_face_thread();
    void new_even_thread();
    void new_eyes_thread();
    void new_back_thread();
    void new_all_thread();
    void reselect_et();

    bool back_on;
    bool face;
    bool eyes;
    bool back;
    bool even;
    bool fullscreen;

    QPixmap m_qt_pm_back;
    QPixmap m_qt_pm_eyes;
    QPixmap m_qt_pm_face;

    QString face_str;
    QString eyes_str;
    QString back_str;
    QString even_str;

    std::thread* reselect_all;
    std::thread* reselect1;
    std::thread* reselect2;
    std::thread* reselect3;
    std::thread* reselect4;

    QString device;
    QTimer* m_timer;

    lsl_face_receiver receiver_face;
    lsl_back_receiver receiver_back;
    lsl_eyes_receiver receiver_eyes;
    lsl_even_receiver receiver_even;

    QPoint d;

    void release_all_receivers();
    void keyPressEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    bool eventFilter(QObject *watched, QEvent *event);
};

#endif

#ifndef LSL_BACK_RECEIVER_H
#define LSL_BACK_RECEIVER_H

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <thread>
#include "lsl_receiver.h"
#include "lsl_video.h"
#include <QImage>
class lsl_back_receiver
{
public:
    lsl_back_receiver();
    bool isNewFrameDone();
    void start(std::string device_id);
    void stop();
    bool startVideoSaving();
    void stopVideoSaving();
    bool isVideoStarted();
    bool isReceiverStarted();
    bool isResolved();
    QImage& getLastImage();

private:
    QImage qt_img;
    std::string eye_tracker_id;
    cv::Size raw_frame_size;
    cv::Size raw_view_size;
    cv::Mat frame_raw;
    cv::Mat frame_view;
    int video_cnt;
    int video_codec;
    cv::VideoWriter writer;
    bool resolved_signal;
    bool started_receiver;
    bool started_video_saving;
    bool ena_work;
    bool thr_working;
    bool video_working;
    bool done;
    void main_thread();
    std::thread* thread_p;
};

#endif

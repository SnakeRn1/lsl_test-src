QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += main.cpp choose.cpp lsl_face_receiver.cpp lsl_back_receiver.cpp lsl_eyes_receiver.cpp lsl_video.cpp lsl_even_receiver.cpp
HEADERS += choose.h lsl_face_receiver.h lsl_back_receiver.h lsl_eyes_receiver.h lsl_even_receiver.h
FORMS += choose.ui
VERSION = 1.0.5

win32 {
INCLUDEPATH += C:\OpenCV\include C:\lsl\include
LIBS += -LC:\OpenCV\x64 -LC:\lsl\x64
LIBS += -lopencv_core451 -lopencv_imgproc451 -lopencv_imgcodecs451 -lopencv_videoio451 -llsl
TARGET = ../../lsl_test_x64/lsl_test_x64
RC_ICONS += imgs/ico.ico
QMAKE_TARGET_COMPANY = "SnakeR Soft"
QMAKE_TARGET_PRODUCT = "lsl_test_x64"
QMAKE_TARGET_DESCRIPTION = "lsl_test_x64"
QMAKE_TARGET_COPYRIGHT = "Copyright © 2021 Horuzhiy Ruslan"
}

unix {
INCLUDEPATH += /usr/include/opencv4 /usr/include/lsl /usr/local/include/opencv4 /usr/local/include/lsl
QMAKE_LFLAGS += -no-pie
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    curs.qrc

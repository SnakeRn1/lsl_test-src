#ifndef EVENTS_H
#define EVENTS_H

#include "lsl_cpp_wrapper.h"
#include <string>

namespace lsl {


class events_t
{
public:
    static const int channels = 1;
    double timestamp = 0.0;
    std::string message;


public:
    // lsl_sender interface
    static lsl::stream_info make_info(const std::string & name, const std::string & src_id, int sample_rate);
    std::string* data() { return & message; }
    const std::string* cdata() const { return & message; }
};


} //namespace lsl

#endif // EVENTS_H

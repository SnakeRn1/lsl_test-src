#include "lsl_face_receiver.h"

lsl_face_receiver::lsl_face_receiver()
{
    started_receiver = false;
    started_video_saving = false;
    ena_work = true;
    done = false;
    thr_working = false;
    video_working = false;
    raw_frame_size = cv::Size(516,386);
    frame_raw = cv::Mat(raw_frame_size,CV_8UC1,cv::Scalar(0));
    raw_view_size = cv::Size(400,300);
    qt_img = QImage(QSize(raw_view_size.width,raw_view_size.height),QImage::Format_Grayscale8);
    frame_view = cv::Mat(raw_view_size,CV_8UC1,qt_img.bits());
    frame_view.setTo(0);
    video_codec = cv::VideoWriter::fourcc('X','V','I','D');
    video_cnt = 0;
}

void lsl_face_receiver::start(std::string device_id)
{
    ena_work = true;
    done = false;
    thr_working = false;
    video_working = false;
    resolved_signal = false;
    resolved_aux = false;
    eye_tracker_id = device_id;
    //mutex
    if (!started_receiver)
    {
        thread_p = new std::thread(&lsl_face_receiver::main_thread, this);
        started_receiver = true;
    }
}

bool lsl_face_receiver::startVideoSaving ()
{
    if(!started_video_saving&&!writer.isOpened())
    {
        std::string filename = eye_tracker_id+"number"+std::to_string(video_cnt)+"_last_saved_video_face.mkv";
        if(writer.open(filename,video_codec,25,frame_raw.size(),false))
        {
            started_video_saving = true;
            return true;
        }
        else
            return false;
    }
    else return false;
}

void lsl_face_receiver::stopVideoSaving ()
{
    started_video_saving = false;
    while(video_working)
    {
        std::cout<<"now_waiting_video_saving"<<std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    if(writer.isOpened())
        writer.release();
    video_cnt ++;
}

void lsl_face_receiver::stop()
{
    //mutex
    resolved_signal = false;
    resolved_aux = false;
    done = false;
    ena_work = false;
    if (started_receiver)
    {
        thread_p->join();
        delete thread_p;
        started_receiver = false;
    }
    if(started_video_saving)
    {
        if(writer.isOpened())
            writer.release();
        started_video_saving = false;
        video_cnt ++;
    }
    ena_work = false;
}

void lsl_face_receiver::main_thread()
{
    lsl::variable_video_frame_t frame(388800);
    lsl::landmarks_aux_t aux_points;

    lsl_receiver<lsl::variable_video_frame_t> cam_video;
    cam_video.set_name("eyetracker.face.video",eye_tracker_id);
    cam_video.start();

    lsl_receiver<lsl::landmarks_aux_t> cam_aux;
    cam_aux.set_name("eyetracker.landmarks_aux",eye_tracker_id);
    cam_aux.start();

    bool resolved_face = false;
    bool resolved_laux = false;

    while(ena_work)
    {
        resolved_face = cam_video.resolve(0.1);
        resolved_laux = cam_aux.resolve(0.1);
        if(resolved_face) break;
    }

    if(resolved_face)
    {
        resolved_signal = true;
        if(resolved_laux) resolved_aux = true;
        while (ena_work)
        {
            //bool aux_input = false;
            //if (cam_aux.receive(aux_points, 0.0)) aux_input = true;

            if (cam_video.receive(frame, 0.0))
            {
                thr_working = true;
                std::string* s = frame.data();
                char* data = const_cast<char*>(s->data());
                size_t size_buf = s->size();
                uchar* buf_for_decoding_ptr = reinterpret_cast<uchar*>(data);
                std::vector<uchar> buf_for_decoding;
                buf_for_decoding.assign(buf_for_decoding_ptr,buf_for_decoding_ptr+size_buf);
                imdecode(buf_for_decoding,cv::IMREAD_GRAYSCALE,&frame_raw);
                if (frame_raw.cols == 1280) resize (frame_raw(cv::Rect(cv::Point((1280-960)/2,0),cv::Size(960,720))),frame_view,raw_view_size);
                else resize(frame_raw,frame_view,raw_view_size);
                if(cam_aux.receive(aux_points, 0.0))
                {
                    float dev = 5.16f;
                    cam_aux.receive(aux_points, 0.0);
                    float d = aux_points.distance*0.001f;
                    cv::Point2f p1(aux_points.right_eye_center_x/dev,aux_points.right_eye_center_y/dev);
                    cv::Point2f p2(aux_points.left_eye_center_x/dev,aux_points.left_eye_center_y/dev);
                    cv::Size2f s (cvRound(20/d),cvRound(10/d));
                    float a = aux_points.head_pose_eul_z*57.2958f;
                    cv::RotatedRect r1(p1,s,a);
                    cv::RotatedRect r2(p2,s,a);
                    cv::Point2f vertices2f1[4];
                    r1.points(vertices2f1);
                    cv::Point2f vertices2f2[4];
                    r2.points(vertices2f2);

                    std::vector<std::vector<cv::Point>> to_draw(2);
                    for(int i = 0; i < 4; ++i)
                    {
                        to_draw.at(0).push_back(vertices2f1[i]);
                        to_draw.at(1).push_back(vertices2f2[i]);
                    }
                    cv::drawContours(frame_view,to_draw,-1,cv::Scalar(255),2,cv::LINE_AA);
                    cv::line(frame_view,cv::Point(0,80),cv::Point(frame_view.cols-1,80),cv::Scalar(255));
                    cv::line(frame_view,cv::Point(0,frame_view.rows-80),cv::Point(frame_view.cols-1,frame_view.rows-80),cv::Scalar(255));
                    cv::RotatedRect rect;
                }

                thr_working = false;
                if(started_video_saving)
                {
                    video_working = true;
                    writer<<frame_raw;
                    video_working = false;
                }
                if(ena_work) done = true;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(5));
        }
    }
    cam_video.stop();
    cam_aux.stop();
}

QImage& lsl_face_receiver::getLastImage()
{
    if(!thr_working)
    {
        return qt_img;
    }
    else
    {
        while(thr_working) std::this_thread::sleep_for(std::chrono::milliseconds(1));
        return qt_img;
    }
}

#include "lsl_rect_receiver.h"

lsl_rect_receiver::lsl_rect_receiver()
{
    decimator = 4;
    started_receiver = false;
    ena_work = true;
    rect_done = false;
    thr_working = false;
    width = static_cast<int>(2064/decimator);
    height = static_cast<int>(1544/decimator);
    raw_frame_size = cv::Size(width,height);
    m_qt_img_rect = QImage(QSize(width,height),QImage::Format_RGB888);
    frame_rect_raw = cv::Mat(raw_frame_size,CV_8UC3,m_qt_img_rect.bits());
    frame_rect_raw.setTo(0);
}

bool lsl_rect_receiver::isNewRectDone()
{
    return rect_done;
}

void lsl_rect_receiver::start(std::string device_id)
{
    ena_work = true;
    rect_done = false;
    thr_working = false;
    eye_tracker_id = device_id;
    //mutex
    if (!started_receiver)
    {
        t_rect_thread = new std::thread(&lsl_rect_receiver::rect_thread, this);
        started_receiver = true;
    }
}

void lsl_rect_receiver::stop()
{
    //mutex
    if (started_receiver)
    {
        ena_work = false;
        t_rect_thread->join();
        delete t_rect_thread;
        started_receiver = false;
    }
    ena_work = false;
}

cv::Mat& lsl_rect_receiver::getNewRawImg()
{
    if(rect_done)
    {
        if(!thr_working)
            frame_rect_raw.copyTo(last_frame_rect);
        rect_done = false;
    }
    return last_frame_rect;
}

cv::Mat& lsl_rect_receiver::getNewResizedImg(int wi, int he)
{
    if(rect_done)
    {
        if(!thr_working)
            resize(frame_rect_raw,frame_rect_maxi,cv::Size(wi,he),0,0,cv::INTER_CUBIC);
        rect_done = false;
    }
    return frame_rect_maxi;
}

bool lsl_rect_receiver::isReceiverStarted()
{
    return started_receiver;
}

void lsl_rect_receiver::matPrint(cv::Mat &img, int lineOffsY, const std::string &ss)
{
    int fontFace = cv::FONT_HERSHEY_COMPLEX;
    double fontScale = 0.5;
    int fontThickness = 1;
    cv::Size fontSize = cv::getTextSize("T[]", fontFace, fontScale, fontThickness, nullptr);
    cv::Point org;
    org.x = 1;
    org.y = 3 * fontSize.height * (lineOffsY + 1) / 2;
    putText(img, ss, org, fontFace, fontScale, cv::Scalar(0), 3*fontThickness, 16);
    putText(img, ss, org, fontFace, fontScale, cv::Scalar(255), fontThickness, 16);
}

void lsl_rect_receiver::et_print(cv::Mat &img, int lineOffsY, const std::string &ss)
{
    int fontFace = cv::FONT_HERSHEY_COMPLEX;
    double fontScale = 1.2;
    int fontThickness = 1;
    cv::Size fontSize = cv::getTextSize("T[]", fontFace, fontScale, fontThickness, nullptr);
    cv::Point org;
    org.x = 1;
    org.y = 3 * fontSize.height * (lineOffsY + 1) / 2;
    putText(img, ss, org, fontFace, fontScale, cv::Scalar(0), 3*fontThickness, 16);
    putText(img, ss, org, fontFace, fontScale, cv::Scalar(255), fontThickness, 16);
}

void lsl_rect_receiver::rect_thread()
{
    lsl::roi_position_t position;
    lsl_receiver<lsl::roi_position_t> roi_receiver;
    roi_receiver.set_name("eyetracker.roi",eye_tracker_id);
    roi_receiver.start();

    bool resolved = false;

    while(ena_work)
    {
        resolved = roi_receiver.resolve(0.02);
        if(resolved)
        {
            break;
        }
    }

    if(resolved)
    {
        while (ena_work)
        {
            if (roi_receiver.receive(position, 0.0))
            {
                thr_working = true;
                frame_rect_raw.setTo(cv::Scalar(0));

                cv::Rect received_rect( static_cast<int>(position.rect_x/decimator),
                                        static_cast<int>(position.rect_y/decimator),
                                        static_cast<int>(position.rect_width/decimator),
                                        static_cast<int>(position.rect_height/decimator)    );

                cv::Point left_eye(     static_cast<int>(position.eye_left_x/decimator),
                                        static_cast<int>(position.eye_left_y/decimator)     );

                cv::Point right_eye(    static_cast<int>(position.eye_right_x/decimator),
                                        static_cast<int>(position.eye_right_y/decimator)    );

                cv::rectangle(frame_rect_raw,received_rect,cv::Scalar(255),2,cv::LINE_AA);
                cv::circle(frame_rect_raw,left_eye,5,cv::Scalar(255),2,cv::LINE_AA);
                cv::circle(frame_rect_raw,right_eye,5,cv::Scalar(255),2,cv::LINE_AA);

                std::ostringstream txt;
                txt<<"dist_r: "<<position.eye_right_distance<<" mm; dist_l: "<<position.eye_left_distance<<" mm";
                matPrint(frame_rect_raw,0,txt.str());
                thr_working = false;
                rect_done = true;

            }
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
    roi_receiver.stop();
}

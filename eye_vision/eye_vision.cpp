#include "lsl_eyes_receiver.h"
#include "signal.h"
#include "et_params.h"

using namespace std;
using namespace cv;

volatile bool ena_work = true;

void my_signal_handler(int)
{
    ena_work = false;
}

int main()
{
    signal(SIGINT, my_signal_handler);
    signal(SIGTERM, my_signal_handler);

    et_params name_container(true,false);
    lsl_eyes_receiver receiver_eyes;
    string dev(name_container.getEyeTrackerID());

    string win_eyes_name_str = "eyes_" + dev;

    namedWindow(win_eyes_name_str);

    receiver_eyes.start(dev);
    receiver_eyes.startVideoSaving();

    while (ena_work)
    {
        if(receiver_eyes.isNewEyesDone())
        {
             cv::imshow(win_eyes_name_str,receiver_eyes.getNewResizedImg(receiver_eyes.width*2,receiver_eyes.height*2));
        }

        int key = cv::waitKey(5);

        if (key==27)
        {
            receiver_eyes.stop();
            break;
        }
    }
    destroyAllWindows();
    return 0;
}



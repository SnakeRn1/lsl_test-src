TARGET = /home/nvidia/.bin/eye_vision
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_LFLAGS += -no-pie
CONFIG += c++17

INCLUDEPATH += /usr/include/opencv4
INCLUDEPATH += /usr/include/lsl
INCLUDEPATH += ../lsl_develop

SOURCES += \
        eye_vision.cpp \
        lsl_eyes_receiver.cpp \
        ../lsl_develop/lsl_video.cpp \
        ../lsl_develop/et_params.cpp

LIBS += -lopencv_core \
        -lopencv_imgcodecs \
        -lopencv_video \
        -lopencv_videoio \
        -lopencv_imgproc \
        -lopencv_highgui \
        -lopencv_photo \
        -llsl64

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
